<?php

namespace App\Http\Controllers;

use App\Http\Models\DAO\ShopDAO;
use App\Http\Models\DTO\ResponseDTO;
use App\Http\Models\Product;
use App\Http\Models\Shop;
use App\Http\Models\TopShop;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;

/*
This class created by Mr.Seng Makara
on 01-05-2020
*/
class MainController
{
    static $res_code = 200;
    static $msg = 'success';

    public function loadHomePage()
    {
        try {
            /* Get all top shop status active */
            $topShop = TopShop::join('shop', 'shop_id', '=', 'top_shop.shop__id')
                ->where('top_shop.status', '=', 1)->get();
            $listTopShopId = [];
            if ($topShop != null) {
                foreach ($topShop as $shodId) {
                    $listTopShopId[] = $shodId['shop__id'];
                }
            }

            /* Get all shop status active and not top shop */
            $allShops = Shop::whereNotIn('shop_id', $listTopShopId)
            -> where('status', '=', 1)->get();

            ResponseDTO::$RESPONSE['STATUS_CODE'] = 'S00001';
            ResponseDTO::$RESPONSE['MESSAGE'] = self::$msg;
            ResponseDTO::$RESPONSE['RESPONSE_CODE'] = self::$res_code;
            ResponseDTO::$RESPONSE['DATA'] = [
                "top_shop" => $topShop,
                "all_shop" => $allShops
            ];
        } catch (\Exception $e){
            return $e->getMessage();
        }
        return ResponseDTO::response();
    }

    public function getAllShop()
    {
        try {
            /* Get all Shop status active */
            $allShop = ShopDAO::getAllShop();

            ResponseDTO::$RESPONSE['STATUS_CODE'] = 'S00001';
            ResponseDTO::$RESPONSE['MESSAGE'] = self::$msg;
            ResponseDTO::$RESPONSE['RESPONSE_CODE'] = self::$res_code;
            ResponseDTO::$RESPONSE['DATA'] = [
                "all_shop" => $allShop
            ];
        } catch(\Exception $e) {
            return $e->getMessage();
        }
        return ResponseDTO::response();
    }

    public function getOwnShop($accountId) {
        try {
            /* Get shop by user_account_id */
            $shopInfo = DB::table('user_account as ua')
                ->join('user_profile as up', 'up.user_account__id', '=', 'ua.user_account_id')
                ->join('shop as s', 's.user_account__id', '=', 'ua.user_account_id')
                ->select('up.first_name', 'up.last_name', 'up.contact', 'up.province', 'up.district',
                    'up.commune', 'up.village', 's.shop_name', 's.logo', 's.description', 's.shop_id')
                ->where('ua.user_account_id', $accountId)
                ->where('ua.status', 1)
                ->first();
            if ($shopInfo != null) {
                /* Get all product by shop_id */
                $product = Product::select('pro_name', 'price', 'color', 'description')
                    ->where('shop__id', $shopInfo->shop_id)
                    ->where('status', 1)
                    ->get();
            } else {
                ResponseDTO::$RESPONSE['STATUS_CODE'] = 'M00001';
                ResponseDTO::$RESPONSE['MESSAGE'] = 'Agent not found.';
                return ResponseDTO::response();
            }

            ResponseDTO::$RESPONSE['STATUS_CODE'] = 'S00001';
            ResponseDTO::$RESPONSE['MESSAGE'] = self::$msg;
            ResponseDTO::$RESPONSE['RESPONSE_CODE'] = self::$res_code;
            ResponseDTO::$RESPONSE['DATA'] = [
                "shop_info" => $shopInfo,
                "all_product" => $product
            ];
            return ResponseDTO::response();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getHello(){
        $client = new \GuzzleHttp\Client();
//        $res = $client->get("http://localhost:8443/game/lotto/hello", [
//            'verify'=>false,
//            'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
//            'body' => ''
//        ]);
        $res = $client->request('GET', 'http://localhost:8443/game/lotto/hello');
        ResponseDTO::$RESPONSE['STATUS_CODE'] = 'S00001';
        ResponseDTO::$RESPONSE['MESSAGE'] = $res->getBody()->getContents();
        return ResponseDTO::response();
    }

    public function insertCategory(Request $request){
        $data = $request->input();
        $catInsert = new Category();
        $catInsert->cat_name = $data['cat_name'];
        $catInsert->status = 1;
        $catInsert->description = $data['description'];
        $catInsert->created_at = Carbon::now();
        $catInsert->save();
    }

}
