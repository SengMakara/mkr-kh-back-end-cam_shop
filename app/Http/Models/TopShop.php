<?php


namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class TopShop extends Model
{
    protected $table = "top_shop";
    protected $fillable = [
        "shop__id",
        "status",
        "created_at",
        "updated_at"
    ];
}
