<?php


namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "category";
    protected $fillable = [
        "cat_id",
        "cat_name",
        "status",
        "description",
        "created_at",
        "updated_at"
    ];
}
