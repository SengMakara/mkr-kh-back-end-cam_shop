<?php


namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $table = "user_profile";
    protected $fillable = [
        "user_profile_id",
        "user_account__id",
        "first_name",
        "last_name",
        "status",
        "created_at",
        "updated_at",
        "contact",
        "province",
        "district",
        "commune",
        "village"
    ];
}
