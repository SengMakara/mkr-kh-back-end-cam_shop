<?php


namespace App\Http\Models\DTO;


class ResponseDTO
{
    public static $RESPONSE = [
        'STATUS_CODE' => 'failed',
        'MESSAGE'=> '',
        'DATA'=>[],
        'RESPONSE_CODE'=> 500,
        'RESPONSE_HEADER'=> 'application/json;charset=utf-8',
    ];

    public static function response(){
        $res = [
            'status'=> [
                "error_code"=>self::$RESPONSE['STATUS_CODE'],
                "message"=>self::$RESPONSE['MESSAGE']
            ],
            'data'=> self::$RESPONSE['DATA']
        ];
        return Response()->json($res, self::$RESPONSE['RESPONSE_CODE'])->header('Content-Type', self::$RESPONSE['RESPONSE_HEADER']);
    }
}
