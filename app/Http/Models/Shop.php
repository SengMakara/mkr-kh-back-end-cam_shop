<?php


namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $table = "shop";
    protected $fillable = [
        "shop_id",
        "user_account__id",
        "shop_name",
        "logo",
        "description",
        "status",
        "created_at",
        "updated_at"
    ];
}
