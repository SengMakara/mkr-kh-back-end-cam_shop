<?php


namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "product";
    protected $fillable = [
        "cat__id",
        "shop__id",
        "pro_name",
        "price",
        "color",
        "description"
    ];
}
