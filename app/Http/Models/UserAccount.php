<?php


namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class UserAccount extends Model
{
    protected $table = "user_account";
    protected $fillable = [
        "user_account_id",
        "user_account_type__id",
        "status",
        "created_at",
        "updated_at",
        "password",
        "reason"
    ];

}
