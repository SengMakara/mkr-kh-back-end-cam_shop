<?php


namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class UserAccountType extends Model
{
    protected $table = "user_account_type";
    protected $fillable = [
        "user_account_type_id",
        "account_type",
        "status",
        "created_at",
        "updated_at"
    ];
}
