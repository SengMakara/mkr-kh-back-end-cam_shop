<?php


namespace App\Http\Models\DAO;


use App\Http\Models\Shop;

class ShopDAO
{
    public static function getAllShop(){
        return Shop::where('status', '=', 1)->get();
    }
}
