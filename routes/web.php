<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServicePgetHellorovider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "MainController@loadHomePage");
Route::get('all_shop', "MainController@getAllShop");
Route::get('own_shop/{accountId}', "MainController@getOwnShop");
Route::get('hello', "MainController@getHello");

